/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{ //สร้างคลาสเพื่อสร้าง fn ที่ทำการ Action กับ Button

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }


}

/**
 *
 * @author focus
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
         JLabel lblYourName = new JLabel("Your name : ");
         lblYourName.setSize(80, 20);
         lblYourName.setLocation(5, 5); //กำหนดตำแหน่ง
         lblYourName.setBackground(Color.WHITE);
         lblYourName.setOpaque(true); //แสดงสีของ BG ที่กำหนด
         
         JTextField txtYourName = new JTextField();
         txtYourName.setSize(200, 20);
         txtYourName.setLocation(90, 5);
         
         JButton btnLogin = new JButton("Login");
         btnLogin.setSize(80, 20);
         btnLogin.setLocation(90, 40);
         
         MyActionListener myActionListener = new MyActionListener();
         btnLogin.addActionListener(myActionListener);//ถ้ากดปุ่มแล้ว จะสั่งให้ Fn myActionListener ทำงานด้วย
         btnLogin.addActionListener(new HelloMe());//ถ้ากดปุ่มแล้ว จะสั่งให้ Fn HelloMe ทำงานด้วย
         
         ActionListener actionListener = new ActionListener(){ // {} คือการสร้าง Anonymous class คือ 
                                                                                        //Class ที่สามารถ Imprements ส่วนที่ยังไม่เสร็จมาแก้ไขต่อได้                                                                                                                    
            @Override
            public void actionPerformed(ActionEvent e) {// เรียกใช้ Method : MyActionListener myActionListener
                                                                                             // = new MyActionListener(); ทั้งหมด
                System.out.println("Anonymous Class : Action");
            }
         };
         btnLogin.addActionListener(actionListener);
         
                 
         JLabel lblHello = new JLabel("Hello ...", JLabel.CENTER);
         lblHello.setSize(200, 20);
         lblHello.setLocation(90, 100); //กำหนดตำแหน่ง
         lblHello.setBackground(Color.WHITE);
         lblHello.setOpaque(true); //แสดงสีของ BG ที่กำหนด
         
         frmMain.setLayout(null);// ใส่เป็น null เพื่อที่เราจะได้จัดองค์ประกอบต่างๆเอง
         
         frmMain.add(lblYourName);
         frmMain.add(txtYourName);
         frmMain.add(btnLogin);
         frmMain.add(lblHello);
         
         btnLogin.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name =txtYourName.getText();//นำข้อมูลที่กรอกใน ช่องText มาเก็บไว้
                lblHello.setText("Hello : " + name);//ให้ Label Text Hello มีข้อมูลที่เรากรอกลงไป
            }
         });
         
         frmMain.setVisible(true);
    
    }

    @Override
    public void actionPerformed(ActionEvent e) {// Implement
        System.out.println("HelloMe : Action");
    }
}
